import info.debatty.java.stringsimilarity.Levenshtein
import khttp.get
import org.json.JSONObject
import java.io.File

private const val clientID = "VSNYGNWm57"
private const val googleAPI = "AIzaSyA-N_4y9BGaPXe6s4602ylRedUhduuJfbk"
private const val googleQuizletSearchEngine = "003020429252225532380:uz20pil1g-s"
private val stringComparator = Levenshtein()
private val dashes = "-".repeat(20)

fun main(args: Array<String>) {
    // val path = File(System.getProperty("user.dir"))
//    val fileName = "Oz.the.Great.and.Powerful.2013.1080p.BluRay.x264.YIFY.mp4"
//    val invalidParts = "1080p 720p BluRay x264 YIFY".split(" ")
//    val parts = fileName.split(".").filter { !invalidParts.contains(it) }
    while (true) {
        println("$dashes\nWhat's your question?")
        getAnswers(readLine()!!).forEach(::println)
    }
}

fun getAnswers(question: String): List<String> {
    val answersByQuestions = mutableMapOf<String, String>() // wish this could be one line not 2
    getQuizletIDs(question).map(::getJSON).map(::getAnswersByQuestions).forEach(answersByQuestions::putAll)

    val new = "$dashes $dashes NEW ANSWER $dashes $dashes"
    return answersByQuestions.map { "$new\n${it.key}\n$dashes\n${it.value}\n" }.filter { it.contains(question) }
}

fun getQuizletIDs(query: String): List<Int> =
    get("https://www.googleapis.com/customsearch/v1?key=$googleAPI&cx=$googleQuizletSearchEngine&q=${query.safeURL()}")
        .jsonObject.getJSONObjects("items")
        .map { it.getString("link") }
        .filter { it.contains("://quizlet.com/") }
        .mapNotNull { it.split("/")[3].toIntOrNull() }

fun getJSON(id: Int) = get("https://api.quizlet.com/2.0/sets/$id?client_id=$clientID&whitespace=1").jsonObject

fun getAnswersByQuestions(jObj: JSONObject) = jObj.getJSONObjects("terms").map {
    it.getString("term") to it.getString("definition")
}.toMap()

fun JSONObject.getJSONObjects(s: String) = this.getJSONArray(s).map { it as JSONObject }
fun String.safeURL() = this.replace(" ", "%20")